var mouseVelocityGauge;

function SegReached(name) {
  this.name = name;
  this.domReference;
  this.attributes = {
    name: "div",
    className: "segment-reached animated slideInDown"
  }
  this.insertEl();
}

SegReached.prototype.insertEl = function() {
  var container = document.getElementById("segment-reached-container"),
  segDiv = document.createElement(this.attributes.name);
  segDiv.className = this.attributes.className;
  segDiv.innerHTML = this.name;
  container.insertBefore(segDiv, container.firstChild);
}

function updateTimeOnPageDiv(time) {
  document.getElementById("time-on-page").innerHTML = time;
}

function updatePagesViewedDiv(time) {
  document.getElementById("times-page-viewed").innerHTML = time;
}

function updateScrollDepthDiv(depth) {
  document.getElementById("scroll-depth").innerHTML = (depth + "%");
}

function updateMouseVelocityDiv(v) {
  if (isNaN(v)) {
    return;
  }
  if (v > 1000) {
    v = 1000;
  }
  mouseVelocityGauge.set(v);
}

function updateSessionTTL(ttl) {
  document.getElementById("session-ttl").innerHTML = ttl;
}

function receiveMessage(event) {
  switch (event.data.type) {
    case 1:
      new SegReached(event.data.name);
      break;
    case 2:
      updateTimeOnPageDiv(event.data.metric);
      break;
    case 3:
      updatePagesViewedDiv(event.data.metric);
      break;
    case 4:
      updateScrollDepthDiv(event.data.metric);
      break;
    case 5:
      updateMouseVelocityDiv(event.data.metric);
      break;
    case 6:
      updateSessionTTL(event.data.metric);
      break;
    default:
      break;
  }
}

function createGauge(el, gaugeMax) {
  var opts = {
    lines: 12, // The number of lines to draw
    angle: 0.15, // The length of each line
    lineWidth: 0.44, // The line thickness
    pointer: {
      length: 0.9, // The radius of the inner circle
      strokeWidth: 0.035, // The rotation offset
      color: "#000000" // Fill color
    },
    limitMax: "false",   // If true, the pointer will not go past the end of the gauge
    colorStart: "#FF7939",   // Colors
    colorStop: "#FF7939",    // just experiment with them
    strokeColor: "#FF7939",   // to see which ones work best for you
    generateGradient: false
  },
  target = document.getElementById(el), // your canvas element
  gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
  gauge.maxValue = gaugeMax; // set max gauge value
  gauge.animationSpeed = 55; // set animation speed (32 is default value)
  return gauge;
}

function init() {
  mouseVelocityGauge = createGauge("mouse-velocity-gauge", 1000);
  window.addEventListener("message", receiveMessage, false);
  window.opener.postMessage("Attently::demo-window-loaded", "*");
}

init();
