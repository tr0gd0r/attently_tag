import { Utils } from "./util.js"

class _PubSub {

  constructor() {
    this.callbacks = {};
  }

  subscribe(event, callback, context, once) {
    var cb = {
      fn: Utils.makeSafe(callback, context),
      once: once,
      alreadyExecuted: false
    }
    if (this.callbacks[event]) {
      this.callbacks[event].push(cb);
    } else {
      this.callbacks[event] = [cb];
    }
  }

  publish(event, eventData) {
    var cbs = this.callbacks[event];
    if (!cbs) {
      return;
    }
    for (var cb of cbs) {
      if ((cb.once === true && cb.alreadyExecuted === false) || cb.once !== true) {
        cb.alreadyExecuted = true;
        cb.fn(eventData);
      }
    }
  }

}

export let PubSub = new _PubSub();
