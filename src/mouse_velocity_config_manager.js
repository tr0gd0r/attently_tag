import { Utils } from "./util.js"
import { PubSub } from "./pubsub.js"

export class MouseVelocityConfigManager {
  
  constructor() {
    this.mouseVelocityConfigs;
    this.shouldCheckMouseVelocity = false;
    this.listenerID;
    this.eventsPerTickPeriod = 0;
    this.eventsInLastTickPeriod = 0;
    this.lastEv = {};
    PubSub.subscribe("configs-loaded", this.loadMouseVelocityConfigs, this);
    PubSub.subscribe("init", this.getSessionVars, this);
  }

  getSessionVars(sessionVars) {
    this.isDemoSession = sessionVars.isDemoSession;
  }

  loadMouseVelocityConfigs(loadedConfigs) {
    this.mouseVelocityConfigs = Utils.getObjectProperty(loadedConfigs, "mouse-velocity", []);
    if (this.mouseVelocityConfigs.length > 1 || this.isDemoSession) {
      this.shouldCheckMouseVelocity = true;
      document.addEventListener("mousemove", this.countMovesPerTick.bind(this));
    }
  }

  countMovesPerTick(ev) {
    var deltaX;
    var deltaY;
    var xyDelta;
    var finalVelocity;
    var acceleration;
    var timeChange;

    if (typeof this.lastEv.screenX === "number") {
      timeChange = ev.timeStamp - this.lastEv.timeStamp;
      deltaX = ev.screenX - this.lastEv.screenX;
      deltaY = ev.screenY - this.lastEv.screenY;
      xyDelta = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
      finalVelocity = Math.round((xyDelta / timeChange) * 1000);
      PubSub.publish("mouse-velocity", finalVelocity);
    }

    this.lastEv.screenX = ev.screenX;
    this.lastEv.screenY = ev.screenY;
    this.lastEv.timeStamp = ev.timeStamp;
  }

}
