export const Utils = {

  areWeInIframe() {
    if (window !== window.top) {
      return true;
    } else {
      return false;
    }
  },

  getCurrentUrl() {
    var res;
    var matches = (location.href).match(/https{0,1}:\/\/(?:www\.){0,1}(.*)/);
    if (matches.length !== 2) {
      return false;
    } else {
      return matches[1];
    }
  },

  makeSafe(fn, context) {
    return function() {
      if (!context) { context = null };
      try {
        fn.apply(context, arguments);
      } catch(e) {
        LOG('makeSafe error', e);
        LOG(fn.toString())
      }
    }
  },

  /*
   * props can be string in which case we return:
   *  object[props]
   * or an array if its a nested object:
   *  object[prop0][prop1]...
   */
  getObjectProperty(object, props, defaultValue) {
    try {
      // props is not an array
      if (!Array.isArray(props)) {
        if (object.hasOwnProperty(props)) {
          return object[props];
        } else {
          return defaultValue;
        }
      }

      var temp = object;
      for (var i = 0; i < props.length; i++) {
        temp = temp[props[i]];
      }

      return temp;
    } catch(e) {
      if (defaultValue) {
        return defaultValue;
      }
    }
  },

  getCookieData() {
    try {
      var trackingObj;
      var c = document.cookie.split(";");
      for (var i = c.length - 1; i >= 0; i--) {
        if (c[i].indexOf("Attently") > -1) {
          trackingObj = JSON.parse(c[i].split("Attently=")[1]);
          return trackingObj;
        }
      }
      return {};
    } catch (e) {
      return {};
    }
  },

  setCookie(cookieObj) {
    document.cookie = "Attently=" + JSON.stringify(cookieObj);
  },

  getTimestamp() {
    return performance && performance.now() || (new Date).getTime();
  }
}
