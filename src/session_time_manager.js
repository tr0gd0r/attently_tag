import { Utils } from "./util.js"
import { PubSub } from "./pubsub.js"
import { Config } from "./config.js"

export class SessionTimeManager {

  constructor() {
    if (Config.sessionTime) {
      PubSub.subscribe("init", this.setSessionStart, this); 
      PubSub.subscribe("tick", this.checkSessionTTL, this); 
    }
  }

  setSessionStart() {
    var cookieData = Utils.getCookieData();
    if (!cookieData["sessionStartTime"]) {
      cookieData["sessionStartTime"] = (new Date).getTime();
    }
    Utils.setCookie(cookieData);
  }

  /*
   * If session has elapsed, clear data
   */
  checkSessionTTL() {
    var now = (new Date).getTime();
    var cookieData = Utils.getCookieData();
    var sessionTTL;

    if ((cookieData["sessionStartTime"] + Config.sessionTime) < now) {
      /* restart session */
      Utils.setCookie({
        "sessionStartTime": now
      });
    }

    /* Session end time - now = session ttl */
    sessionTTL =  Math.round(((cookieData["sessionStartTime"] + Config.sessionTime) - now) / 1000);
    PubSub.publish("session-ttl", sessionTTL)  
  }

}
