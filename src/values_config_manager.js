import { Utils } from "./util.js"
import { PubSub } from "./pubsub.js"

export class ValuesConfigManager {

  constructor() {
    PubSub.subscribe("configs-loaded", this.loadValueConfigs, this); 
  }

  isANumber(char) {
    try {
      var num = Number(char);
      if (isNaN(num)) {
        return false;
      } 
      // if we made it here, it's a number
      return true;
    } catch(e){return false;}
  }

  loadValueConfigs(loadedConfigs) {
    var pageValuesConfigs = Utils.getObjectProperty(loadedConfigs, "extractValue", []);
    pageValuesConfigs.forEach(config => {
      var satisfied,
          extractVal,
          elToExtractFrom = config.el_selector();

      if (!elToExtractFrom) {return false;}
      config.comparison_value = Number(config.comparison_value);

      if (config.type === "number") {
        /* attempt to search for number in string */
        var rawVal = elToExtractFrom.innerText,
            strLen = rawVal.length,
            i,
            num,
            startPosition,
            endPosition;

        for (i = 0; i < strLen; i++) {
          var charIsNumber = this.isANumber(rawVal[i]);
          if (charIsNumber && !startPosition) {
            startPosition = i;
          }

          // purposely will stop on the decimal and only return the integer part
          if (!charIsNumber && startPosition >= 0) {
            endPosition = i;
            break;
          }
        }

        extractVal = Number(rawVal.slice(startPosition, endPosition));
      } else if (config.type === "string") {
        extractVal = elToExtractFrom.innerText;
      }
      
      switch (config.comparison_operator) {
        case ">":
          satisfied = extractVal > config.comparison_value;
          break;
        case "<":
          satisfied = extractVal < config.comparison_value;
          break;
        case "=":
          satisfied = extractVal == config.comparison_value;
          break;
        default:
          break;
      }

      if (satisfied) {
        LOG("reached segment: ", config.name);	
        PubSub.publish("action-happened", {type: config.action, id: config.segment_id});
      }
    })
  }

}
