import { PubSub } from "./pubsub.js"
import { Config } from "./config.js"

export class ConfigLoader {

  constructor() {
    this.configUrl = "http://127.0.0.1:5000/api/config/testing";
    PubSub.subscribe('init', this.loadConfigs, this, true);
  }

  /*
   * url1 must be a singe url string
   * url2 can be a string or an array of url strings or regexes
   */
  doesPageCount(currentURL, allowedURLs) {
    var counts = false;
    allowedURLs.forEach(allowedURL => {
      var result = this.urlTest(currentURL, allowedURL);
      if (result) {counts = true};
    })
    return counts;
  }

  urlTest(currentURL, allowedURL) {
    var re = new RegExp(allowedURL, "i");
    return re.test(currentURL);
  }

  loadConfigs(sessionVariables) {
    this.currentUrl = sessionVariables.currentUrl;
    this.ajax(this.configUrl, res => {
      var config = JSON.parse(res);
      this.parseConfigs(config);
    })    
  }

  parseConfigs(config) {
    var loadedConfigs = {},
        matchedConfigs = 0;
    config.segments.forEach(seg => {
      var urlMatches = this.doesPageCount(this.currentUrl, seg.pages);
      if (urlMatches === false) {return;};
      matchedConfigs++;
      /* convert selector to function */
      if (seg.el_selector) {
        var elSelectorFn = new Function(seg.el_selector);
        seg.el_selector = elSelectorFn;
      }
      if (seg.action in loadedConfigs) {
        loadedConfigs[seg.action].push(seg)
      } else {
        loadedConfigs[seg.action] = [seg]
      }
    })
    if (matchedConfigs === 0) {
      LOG("no configs matched");
    } else {
      LOG("loaded " + matchedConfigs + " configs");
      PubSub.publish("configs-loaded", loadedConfigs); 
      setInterval(() => {
        PubSub.publish("tick", {interval: config.tickTime});
      }, config.tickTime); 
    }
  }

	ajax(url, callback) {
		try {
			var x = new XMLHttpRequest();
			x.open("GET", url, true);
			x.setRequestHeader("Content-type", "application/json; charset=utf-8");
			x.onreadystatechange = function () {
				x.readyState > 3 && callback && callback(x.responseText, x);
			};
			x.send();
		} catch (e) {debugger;}
	}

}

