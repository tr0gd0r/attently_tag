import { Utils } from "./util.js"
import { PubSub } from "./pubsub.js"

export class InteractionConfigManager {

  constructor() {
    PubSub.subscribe("configs-loaded", this.elementInteractionWatcher, this); 
  }

  elementInteractionWatcher(loadedConfigs) {
    var mouseoverConfigs = Utils.getObjectProperty(loadedConfigs, "mouseover", []);
    var clickConfigs = Utils.getObjectProperty(loadedConfigs, "click", []);
    var actionConfigs = [].concat(clickConfigs, mouseoverConfigs);
    actionConfigs.forEach(config => {
      var el = config.el_selector(),
          callbackFn;
      if (!el) {
        LOG("could not find element: ", config);
        // if the page matched but an el coulnd't be found
        // something went wrong like the page is different or something
        // we should signal this back and ideally this would be an alert
        // in the UI or something

        //TRY UNTIL FOUND/MAX LIMIT NEEDED HERE (might not be in DOM)
        return;
      }
      LOG("adding listener: ", config.action, el);
      callbackFn = ((action, configID)  => {
        return e => {
          LOG(action, e.currentTarget);
          PubSub.publish("action-happened", {type: action, id: configID});
        }
      })(config.action, config.segment_id);
      el.addEventListener(config.action, callbackFn);
    });
  }

}
