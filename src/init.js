import { ConfigLoader } from "./config_loader.js"
import { Demo } from "./demo.js"
import { InViewConfigMananger } from "./in_view_config_manager.js"
import { InteractionConfigManager } from "./interaction_config_manager.js"
import { MouseVelocityConfigManager } from "./mouse_velocity_config_manager.js"
import { PagesViewedConfigManager } from "./pages_viewed_config_manager.js"
import { PubSub } from "./pubsub.js"
import { ScrollDepthConfigManager } from "./scroll_depth_config_manager.js"
import { SessionTimeManager } from "./session_time_manager.js"
import { TimeConfigManager } from "./time_config_manager.js"
import { UrlVisitedConfigManager } from "./url_visited_config_manager.js"
import { ValuesConfigManager } from "./values_config_manager.js"
import { Utils } from "./util.js"
import { ElementSelector } from "./element_selector.js"

LOG("starting up!");

var goodToGo = true;

if (Utils.areWeInIframe()) {
  goodToGo = false;
  if (window.name === "attently") {
    /* when element selection in the UI is active, let it know the attently tag in the iframe has loaded */
    window.top.postMessage({msg: "attently::loaded"}, "http://attently.com");
    addEventListener("message", e => {
      if (e.data === "attently::start-selector") {
        new ElementSelector();
      }
    });
  } else {
    LOG("WE ARE IN AN IFRAME. ABORT!!");
  }
}

/*
 * prob need a function to do some feature detection like do we have JSON, localstorage, etc. 
 * would be good to report this back for stats gathering
 */

var currentUrl = Utils.getCurrentUrl();
if (!currentUrl) {
  LOG("CANNOT PARSE URL. ABORT!!");
  goodToGo = false;
}

if (goodToGo) {
  const sessionVariables = {
    currentUrl: currentUrl,
    startTime: (new Date).getTime(),
    isDemoSession: true
  }

  new ConfigLoader()
  new InViewConfigMananger()
  new InteractionConfigManager()
  new MouseVelocityConfigManager()
  new PagesViewedConfigManager()
  new ScrollDepthConfigManager()
  new SessionTimeManager()
  new TimeConfigManager()
  new UrlVisitedConfigManager()
  new ValuesConfigManager()
  
  if (sessionVariables.isDemoSession) {
    new Demo()
  }  

  PubSub.publish("init", sessionVariables);
}

