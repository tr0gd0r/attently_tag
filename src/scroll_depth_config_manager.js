import { Utils } from "./util.js"
import { PubSub } from "./pubsub.js"

export class ScrollDepthConfigManager {

  constructor() {
    this.scrollDepthConfigs;
    PubSub.subscribe("configs-loaded", this.loadScrollDepthConfigs, this); 
  }

  loadScrollDepthConfigs(loadedConfigs) {
    this.scrollDepthConfigs = Utils.getObjectProperty(loadedConfigs, "scroll_depth", []);
    if (this.scrollDepthConfigs.length >= 1) {
      PubSub.subscribe("tick", this.checkScrollDepthConfigs, this); 
    }
  }

  checkScrollDepthConfigs() {
    var config;
    var i;
    var scrollPercent = (document.body.scrollTop / (document.body.scrollHeight - document.documentElement.clientHeight));

    this.scrollDepthConfigs.forEach(config => {
      if (config.completed) {return};
      if (scrollPercent * 100 >= Number(config.threshold)) {
        config.completed = true;
        LOG("reached segment:", config.name);
        PubSub.publish("action-happened", {type: config.action, id: config.segment_id});
      } 
    })
  }

}
