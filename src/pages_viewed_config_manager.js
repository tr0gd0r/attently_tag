import { Utils } from "./util.js"
import { PubSub } from "./pubsub.js"

export class PagesViewedConfigManager {

  constructor() {
    PubSub.subscribe("configs-loaded", this.loadPagesViewedConfigs, this);
    PubSub.subscribe("init", this.getSessionVars, this);
  }

  getSessionVars(sessionVariables) {
    this.currentUrl = sessionVariables.currentUrl;
  }

  loadPagesViewedConfigs(loadedConfigs) {
    var pagesViewedConfigs = Utils.getObjectProperty(loadedConfigs, "pages_viewed", []);
    var cookieData = Utils.getCookieData();
    
    //increment count for current page
    if (cookieData[this.currentUrl]) {
      cookieData[this.currentUrl]["pages_viewed"] += 1;
    } else {
      cookieData[this.currentUrl] = {"pages_viewed": 1, "time": 0};
    }
    Utils.setCookie(cookieData);
    
    //check to see if any configs are satisfied
    pagesViewedConfigs.forEach(config => {
      if (config.completed) {return;}

      var totalVisits = 0;
      var neededVisits = Number(config.number);
      /**
       * for each config regex, check every url we've seen
       * to see if it's a match, if it is, increment totalVisits
       */
      config.pages.forEach(page => {
        for (var url in cookieData) {
          var re = new RegExp(page, "i");
          if (re.test(url)) {
            totalVisits += Utils.getObjectProperty(cookieData, [url, "pages_viewed"], 0);
          }
        }
      })
      if (totalVisits > neededVisits) {
        config.completed = true;
        LOG("reached segment:", config.name)
        PubSub.publish("action-happened", {type: config.action, id: config.segment_id});
      }
    })
  }

}
