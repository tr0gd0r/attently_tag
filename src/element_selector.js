import { Utils } from "./util.js"
import { PubSub } from "./pubsub.js"

export class ElementSelector {

  constructor() {
    var jsUrl = "http://attently.com/attently-element-selector/dist/element-selector.min.js",
        zeptoUrl = "https://cdnjs.cloudflare.com/ajax/libs/zepto/1.2.0/zepto.min.js",
        optimalSelectUrl = "http://attently.com/attently-element-selector/dist/optimal-select.min.js",
        cssUrl = "http://attently.com/attently-element-selector/dist/element-selector.css",
        srcdoc = `<head>
            <script src="${zeptoUrl}"></script>
            <script src="${optimalSelectUrl}"></script>
            <script src="${jsUrl}"></script>
          </head>`
    
    this.loadCSS(cssUrl);
    this.createIframe(srcdoc);
  }

  loadCSS(url) {
    var s = document.createElement("link");
    s.setAttribute("rel", "stylesheet");
    s.setAttribute("type", "text/css");
    document.head.appendChild(s);
    s.setAttribute("href", url);
  }

  createIframe(srcdoc) {
    var i = document.createElement("iframe");
    i.setAttribute("style", "width: 0px; height: 0px; display: none;");
    i.setAttribute("name", "attently-sg-iframe");
    i.setAttribute("srcdoc", srcdoc);
    document.body.appendChild(i);
    this.iframeWin = i.contentWindow;
  }
}

