import { Utils } from "./util.js"
import { PubSub } from "./pubsub.js"

export class Demo {

  constructor() {
    /*
     * If the popup is already open, it doens't open a second but just "makes the connection" and demoWin is a
     * reference to the already open win
     */
    const demoUrl = "http://blerg.com/attently-tag/demo_popup/";
    const strWindowFeatures = "menubar=no,location=no,resizable=yes,scrollbars=no,status=no,toolbar=no,titlebar=no";

    this.demoWin = window.open(demoUrl, "ezseg_demo_win", strWindowFeatures);
    this.configsById = {};
    this.messageCache = [];
    this.demoPopupLoaded = false;
    this.dummyEls = {};

    {
      var hintcss = "//cdnjs.cloudflare.com/ajax/libs/hint.css/2.3.2/hint.min.css";
      var el = document.createElement("link");
      el.setAttribute("href", hintcss);
      el.setAttribute("rel", "stylesheet");
      document.head.appendChild(el);
    }

    window.addEventListener("message", this.receiveMessage.bind(this), false);

    PubSub.subscribe("configs-loaded", this.getConfigNames, this);
    PubSub.subscribe("action-happened", this.sendActionToDemoWin, this);
    PubSub.subscribe("mouse-velocity", this.sendMouseVelocity, this);
    PubSub.subscribe("session-ttl", this.sendSessionTTL, this)
    PubSub.subscribe("inviewtime-create-dummy-el", this.createDummyEl, this)
    PubSub.subscribe("inviewtime-update-dummy-el", this.updateDummyEl, this)
    PubSub.subscribe("tick", this.updateTimeDiv, this);
    PubSub.subscribe("tick", this.updateTimesViewedDiv, this);
    PubSub.subscribe("tick", this.checkScrollDepth, this);
    PubSub.subscribe("init", this.getSessionVars, this);
  }

  getSessionVars(sessionVars) {
    this.currentUrl = sessionVars.currentUrl;
  }

  sendActionToDemoWin(conf) {
    if (this.demoPopupLoaded) {
      var name = this.configsById[conf.id];
      var msg = {};
      msg.name = name;
      msg.type = 1;
      this.demoWin.postMessage(msg, "*");
    } else {
      this.messageCache.push(conf);
    }
  }

  sendCounterToDemoWin(metric, type) {
    var msg = {type, metric}
    if (this.demoPopupLoaded) {
      this.demoWin.postMessage(msg, "*");
    }
  }

  /*
   * create id to name association of segments
   */
  getConfigNames(loadedConfigs) {
    var type;
    for (type in loadedConfigs) {
      loadedConfigs[type].forEach(config => {
        this.configsById[config.segment_id] = config.name;   
      });      
    }
  }

  /*
   * when demo popup win loads, it sends a message.
   * some events are published before popup window has loaded so
   * we cache them until we get a loaded message.
   */
  receiveMessage(ev) {
    if (ev.data === "Attently::demo-window-loaded") {
      this.demoPopupLoaded = true;
      this.messageCache.forEach(conf => this.sendActionToDemoWin(conf));
    }
  }

  updateTimeDiv() {
    var cookieData = Utils.getCookieData();
    var urlData = cookieData[this.currentUrl];
    this.sendCounterToDemoWin(Math.round(urlData["time"]/1000), 2);
  }

  updateTimesViewedDiv() {
    var cookieData = Utils.getCookieData();
    var urlData = cookieData[this.currentUrl];
    this.sendCounterToDemoWin(urlData["pages_viewed"], 3);
  }

  checkScrollDepth() {
    var scrollPercent = (document.body.scrollTop / (document.body.scrollHeight - document.documentElement.clientHeight));
    this.sendCounterToDemoWin(Math.round(scrollPercent*100), 4);
  }

  sendMouseVelocity(v) {
    this.sendCounterToDemoWin(v, 5);  
  }

  sendSessionTTL(ttl) {
    this.sendCounterToDemoWin(ttl, 6);  
  }

  createDummyEl(data) {
    var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    var span = document.createElement("span");

    span.style.width = data.rect.width + "px";
    span.style.height = data.rect.height + "px";
    span.style.top = data.rect.top + document.body.scrollTop + "px";
    span.style.left = data.rect.left + "px";
    span.style.position = "absolute";

    span.classList.add("hint--right");
    span.classList.add("hint--always");
    span.classList.add("hint--info");

    document.body.appendChild(span);

    this.dummyEls[data.id] = span;
  }

  updateDummyEl(data) {
    var el = this.dummyEls[data.id];
    el.setAttribute("aria-label", data.time);
  }

}

