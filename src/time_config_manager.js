import { Utils } from "./util.js"
import { PubSub } from "./pubsub.js"

/**
 * time counting logic:
 *  get time on site data stored in cookie
 *  increment by delta between now and last check
 *  check any configs match
 */
export class TimeConfigManager {

  constructor() {
    this.timeConfigs;
    this.timeAtLastCheck;
    PubSub.subscribe("configs-loaded", this.loadTimeConfigs, this);
    PubSub.subscribe("init", this.getSessionVars, this);
    PubSub.subscribe("tick", this.checkTimeConfigs, this);
  }

  getSessionVars(sessionVariables) {
    this.currentUrl = sessionVariables.currentUrl;
  }

  loadTimeConfigs(loadedConfigs) {
    this.timeConfigs = Utils.getObjectProperty(loadedConfigs, "time_on_site", []);
  }

  checkTimeConfigs() {
    /* not yet loaded*/
    if (!this.timeConfigs) {
      return;
    }
    var now = (new Date).getTime(),
        delta;
    if (this.timeAtLastCheck) {
      delta = now - this.timeAtLastCheck;
    } else {
      delta = 0;
    }
    var cookieData = Utils.getCookieData();
    if (cookieData[this.currentUrl]) {
      cookieData[this.currentUrl]["time"] += delta;
    } else {
      cookieData[this.currentUrl] = {"pages_viewed": 0, "time": 0};
    }
    Utils.setCookie(cookieData);

    this.timeAtLastCheck = now;

    /**
     * If the config is single url, it's a straightforward comparison
     * of current time for that url vs. config's needed time.
     * 
     * If the config matches multiple urls (it's an array and/or regex) then 
     * you need to count the current time for all those matching urls and
     * compare it to the config's needed time.
     */
    this.timeConfigs.forEach(config => {
      if (config.completed) {return;}
      var totalTime = 0,
          neededTime = Number(config.time) * 1000;

      /**
       * for each config regex, check every url we've seen
       * to see if it's a match, if it is, increment totalTime
       */
      config.pages.forEach(allowedUrl => {
        var re = new RegExp(allowedUrl, "i");
        for (var visitedUrl in cookieData) {
          if (re.test(visitedUrl)) {
              totalTime += Utils.getObjectProperty(cookieData, [visitedUrl, "time"], 0);
          }
        }
      })

      if (totalTime >= neededTime) {
        config.completed = true;
        LOG("reached segment:", config.name)
        PubSub.publish("action-happened", {type: config.action, id: config.segment_id});
      }

    })
  }

}
