export const Config = {
  normalizeUrl: false,
  sessionTime: 300000,
  tickTime: 100,
  segments: [{
    action: "url",
    id: 93948392,
    name: "Visited product page",
    device: "all",
    page: [ new RegExp("gap.com/browse/product.do*", "i") ],
    thirdPartyPxls: []
  },
  {
    action: "extractValue",
    type: "int",
    id: 39439,
    name: "Viewed product over $10",
    device: "all",
    page: [ new RegExp("gap.com/browse/product.do*", "i") ],
    extract() {
      return parseFloat(gap.pageProductData.currentMaxPrice.substring(1)) > 10;
    },
    thirdPartyPxls: []
  },
  {
    action: "click",
    id: 9438893,
    name: "Clicked something",
    device: "desktop",
    page: [ new RegExp("gap.com/browse/product.do*", "i") ],
    elFinder() {
      return document.getElementById("topNav");
    },
    thirdPartyPxls: []
  },
  {
    action: "mouseover",
    id: 894403,
    name: "Moused over something",
    device: "desktop",
    page: [ new RegExp("gap.com/browse/product.do*", "i") ],
    elFinder() {
      return document.getElementById("gapLogo_2016");
    },
    thirdPartyPxls: []
  },
  {
    id: 8545645613,
    name: "Spent more than 10 seconds on a product page",
    device: "desktop",
    action: "time_on_site",
    page: [ new RegExp("gap.com/browse/product.do*", "i") ],
    time: 10000,
    thirdPartyPxls: []
  },
  {
    id: 15785625646,
    device: "desktop",
    action: "pages_viewed",
    name: "Viewed more than 10 product pages",
    page: [ new RegExp("gap.com/browse/product.do*", "i") ],
    number: 10,
    lookback: 1800,
    thirdPartyPxls: []
  },
  {
    id: 8384738392,
    device: "desktop",
    action: "scroll_depth",
    name: "Scrolled more than halfway through page",
    threshold: .5,
    page: [ new RegExp("gap.com/browse/product.do*", "i") ],
    thirdPartyPxls: []
  },
  {
    id: 23836738,
    device: "desktop",
    action: "in_view_time",
    name: "product photo fully in view for > 5 seconds",
    viewableAreaThreshold: 1,
    viewableTimeThreshold: 5000,
    elFinder() {
      return document.getElementsByClassName("product-photo")[0];
    },
    page: [ new RegExp("gap.com/browse/product.do*", "i") ],
    thirdPartyPxls: []
  }]
}
