import { Utils } from "./util.js"
import { PubSub } from "./pubsub.js"

export class UrlVisitedConfigManager {

  constructor() {
    PubSub.subscribe("configs-loaded", this.loadVistedUrlConfigs, this); 
  }

  loadVistedUrlConfigs(loadedConfigs) {
    var pageVisitConfigs = Utils.getObjectProperty(loadedConfigs, "url", []);

    pageVisitConfigs.forEach(config => {
      // if the config loaded, that means the URL matched
      LOG("reached segment:", config.name)
      PubSub.publish("action-happened", {type: config.action, id: config.segment_id});
    })
  }
}
