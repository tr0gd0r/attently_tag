import { Utils } from "./util.js"
import { PubSub } from "./pubsub.js"

class InViewCounter {

  constructor(config) {
    var elToWatch = config.el_selector();
    if (!elToWatch) {
      return false;
    }
    this.stopCounting = false;
    this.elToWatch = elToWatch;
    this.viewableAreaThreshold = Number(config.viewableAreaThreshold) / 100;
    this.viewableTimeThreshold = Number(config.viewableTimeThreshold) * 1000;
    this.lastRunTimestamp;
    this.inViewTime = 0;
    this.configID = config.segment_id;
  }

  getInViewPercent() {
    var rect = this.elToWatch.getBoundingClientRect();
    var viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    var viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    var elHeightInView = this.getHeightInView(rect, viewportHeight);
    var elWidthInView = this.getWidthInView(rect, viewportWidth);
    var areaInView = elHeightInView * elWidthInView;
    var elArea = rect.width * rect.height;

    return areaInView / elArea;
  }

  getHeightInView(boundingRect, vh) {
    /* if el bottom is negative or el top exceeds viewoprt height, it's out of view */
    if (boundingRect.bottom <= 0 || boundingRect.top >= vh) {
      return 0;
    }

    /* if el bottom isn't negative but el top is, it's partially out of view */
    if (boundingRect.top < 0) {
      return boundingRect.height - Math.abs(boundingRect.top);
    }

    /* if el bottom exceeds viewport height but el top doesn't, it's partially in view */
    if (boundingRect.top < vh && boundingRect.bottom > vh) {
      return vh - boundingRect.top;
    }

    /* it's fully in view */
    return boundingRect.height;
  }

  getWidthInView(boundingRect, vw) {
    if (boundingRect.right < 0 || boundingRect.left > vw) {
      return 0;
    }

    /* if el right isn't off the screen but el left is, it's partially out of view */
    if (boundingRect.left < 0) {
      return boundingRect.width - Math.abs(boundingRect.left)
    }

    /* if el left isn't off the screen but el right is, it's partially out of view */
    if (boundingRect.right > vw) {
      return vw - boundingRect.left;
    }

    /* it's fully in view */
    return boundingRect.width;
  }
}

export class InViewConfigMananger {

  constructor() {
    this.inViewConfigs;
    this.inViewCounters = [];
    PubSub.subscribe("configs-loaded", this.loadInViewConfigs, this);
  }

  loadInViewConfigs(loadedConfigs) {
    var inViewCounter;
    var config;
    this.inViewConfigs = Utils.getObjectProperty(loadedConfigs, "in_view_time", []);
    this.inViewConfigs.forEach(config => {
      inViewCounter = new InViewCounter(config);
      if (inViewCounter) {
        this.inViewCounters.push(inViewCounter);
        PubSub.publish("inviewtime-create-dummy-el", {
          rect: inViewCounter.elToWatch.getBoundingClientRect(),
          id: inViewCounter.configID
        });
      }
    })
    if (this.inViewCounters.length > 0) {
      PubSub.subscribe("tick", this.checkInViewCounters, this);
    }
  }

  checkInViewCounters() {
    var counter,
        viewableArea,
        now;
    /*
     * for each counter that is running, check if the element's viewable area is
     * greater than the viewableAreaThreshold. If it is, increment the in view time.
     */
    this.inViewCounters.forEach(counter => {
      if (counter.stopCounting) {
        return;
      }
      viewableArea = counter.getInViewPercent();
      now = Utils.getTimestamp();
      if (viewableArea >= counter.viewableAreaThreshold) {
        counter.inViewTime += (now - (counter.lastRunTimestamp || now))
      }
      counter.lastRunTimestamp = now;
      if (counter.inViewTime >= counter.viewableTimeThreshold) {
        counter.stopCounting = true;
        PubSub.publish("action-happened", {type:"in_view_time" , id: counter.configID});
      }
      PubSub.publish("inviewtime-update-dummy-el", {
        id: counter.configID,
        time: Math.min(Math.round(counter.inViewTime), counter.viewableTimeThreshold)
      });
    })
  }

}
