import babel from 'rollup-plugin-babel';

export default {
  entry: 'src/init.js',
  dest: 'attently.js',
  format: 'iife',
  globals: {
    attently: 'Attently'
  },
  plugins: [
    babel({
      exclude: 'node_modules/**',
      presets: 'es2015-rollup'
    })
  ],
};

