#!/bin/bash
set -eu

echo 'START...'
cwd="$( cd "$( dirname "${BASH_SOURCE[0]}"   )" && pwd )"
export PATH=$PATH:$cwd/node_modules/.bin

rollup -c
sed -i.bak 's/LOG(/console.log("Attently: " ,/g' $cwd/attently.js
rm -f $cwd/attently.js.bak
echo 'FINISHED'
